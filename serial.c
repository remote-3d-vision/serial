#include <termios.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>

#include "serial.h"

int serial_init(serial_ctx_t *ctx, char *serial_port_name)
{
    ctx->serial_fd = open(serial_port_name, O_RDWR | O_NOCTTY | O_SYNC);  
    if (ctx->serial_fd < 0)
    {
        fprintf(stderr, "Failed to open serial port: %s. Errno %d\n", serial_port_name, errno);
        return -1;
    }

    struct termios port_settings;

    cfsetospeed(&port_settings, BAUD_RATE);
    cfsetispeed(&port_settings, BAUD_RATE);

    port_settings.c_cflag &= ~PARENB;
    port_settings.c_cflag &= ~CSTOPB;
    port_settings.c_cflag &= ~CSIZE;
    port_settings.c_cflag |= CS8;

    cfmakeraw(&port_settings);
    if (tcsetattr(ctx->serial_fd, TCSANOW, &port_settings) < 0)
    {
        fprintf(stderr, "Failed to set serial attributes. Errno %d\n", errno);
        return -1;
    }

    return 0;
}

void serial_write(serial_ctx_t *ctx, char *message, size_t len)
{
    write(ctx->serial_fd, message, len);
}
