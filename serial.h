#ifndef __SERIAL_H__
#define __SERIAL_H__

#define BAUD_RATE B19200

typedef struct serial_ctx_s
{
    int serial_fd;
} serial_ctx_t;

int serial_init(serial_ctx_t *ctx, char *serial_port_name);
void serial_write(serial_ctx_t *ctx, char *message, size_t len);

#endif
